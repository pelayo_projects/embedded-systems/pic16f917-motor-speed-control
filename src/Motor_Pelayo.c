//Pelayo leguina lopez

//UO246774, 2020

#include "Motor_Pelayo.h"

//Incluye fragmentos de codigo de Gestion_LCD***

///////////////////////////////////////////////////////////////////////////////////////////
//                                 Configuraci�n del LCD
//  Aqu� se definen las direcciones asignadas a cada bit para cada segmento de cada d�gito
 
//
/////////////////////////////////////////////////////////////////////////////////////////
// El de la derecha lo llamamos DIGITO_1, el de la izquierda ser�  el 4
// Segmentos        A        B        C        D        E        F        G
// BITS              b7       b6       b5       b4       b3       b2       b1
#define DIGITO_1   COM0+22, COM0+23, COM2+23, COM3+22, COM2+22, COM1+22, COM1+23
#define DIGITO_2   COM0+6,  COM0+21, COM2+21, COM3+6,  COM2+6,  COM1+6,  COM1+21
#define DIGITO_3   COM0+3,  COM0+11, COM2+11, COM3+3,  COM2+3,  COM1+3,  COM1+11



// En el d�gito de la izquierda  s�lo hay 2 segmentos, con lo que s�lo se podr�a representar un uno o nada
// por tanto, esos 2 segmentos se controlar�n con un �nico bit, que corresponde al bit 2 del registro LCDDATA6
#byte LCDDATA3 = 0x113
#byte LCDDATA6 = 0x116
#byte LCDDATA10 = 0x11A

#bit DIGITO_4_seg_B_C=LCDDATA6.2
//El punto decimal de los voltios 
#bit punto_decimal_voltios=LCDDATA10.3
//Simbolo voltios (V)
#bit unidad_V=LCDDATA3.0

#bit   GO_AD = 0x1F.2   //definimos el bit que lanza las conversiones A/D

//Definimos posiciones de los registros de configuraci�n del m�dulo LCD
#byte LCDCON = 0x107
#byte LCDPS  = 0x108
#byte LCDSE0 = 0x11C
#byte LCDSE1 = 0x11D
#byte LCDSE2 = 0x11E

// A continuaci�n se recoge la tabla de constantes que indican con un uno qu� segmento se debe mostrar y con un cero el que no
// El orden seguido debe coincidir con el definido a la hora de realizar la configuraci�n del LCD: A-B-C-D-E-F-G
// como son 7 bits, el menos significativo se rellena con un cero
byte const Digit_Map[10] =
{0b11111100,0b01100000,0b11011010,0b11110010,0b01100110,0b10110110,0b10111110,0b11100000,0b11111110,0b11110110};
//n-> 0          1          2        3          4          5          6        7           8          9

//Con este puntero recorreremos todas las posiciones a borrar al principio en el m�dulo LCD
char *puntero_borrado;

//Aqu� van los d�gitos de miles, centenas, decenas y unidades de velocidad
int   unidades =        0;
int   decenas  =        0;
int   centenas =        0;
int   miles    =        0;
int   contador_timer0 = 0;

long resultado_AD;   // resultado de la conversi�n (10 bits)

long pulsaciones; // pulsaciones  del tmr1

long v;   // definimos el valor de velocidad

void   Operaciones_unidades(long num)
{
   // Descomponemos el valor de la tension para poder mostrarla en el LCD

   int16 resto;

  
   miles=num/1000;
   
   resto=num%1000;
   
   centenas=resto/100;
   
   resto=resto%100;
   
   decenas=resto/10;
   
   unidades=resto%10;
}

void   Ver_LCD(void)
{     
      // Cargamos los bits de cada segmento con LCD_SYMBOL()
      

      LCD_SYMBOL(DIGIT_MAP[unidades],DIGITO_1);
      LCD_SYMBOL(DIGIT_MAP[decenas],DIGITO_2);
      LCD_SYMBOL(DIGIT_MAP[centenas],DIGITO_3);

      // Digito 4 -> 1 o 0
      if(miles==1)
         DIGITO_4_seg_B_C=1;
      else
         DIGITO_4_seg_B_C=0;
}




#int_RTCC
void RTCC_isr()
{

   //Calculamos el timer para que cuente un segundo:
   //Si precargamos 0, contamos 32,768ms y si contamos eso 30 veces, es
  
   set_timer0(0); // Precarga
   contador_timer0++;
   
   if(contador_timer0==30)
   { // cont==30 porque 1s=32,768 * 30 ms
      // TMR1 para contar pulso, inicialmente a 0
      pulsaciones=get_timer1();
      set_timer1(0);
      v = 30*pulsaciones;  //pasamos pulsaciones a rpm (2 pulsaciones por vuelta) y lo multiplicamos por 60. 
      // PAra poder ver el resultado de la velocidad en LCD:
      Operaciones_unidades(v);
      Ver_LCD();
      contador_timer0=0;  // Reinicio el contador
   }
}


void main()
{

   //Se�al RA0 como anal�gica,  Tad = 4us
   setup_adc_ports(sAN0|VSS_VDD);
   setup_adc(ADC_CLOCK_DIV_32);
   //TMR2
   setup_timer_2(T2_DIV_BY_1,255,1);
   //PWM con duty=0
   //definimos rd2/ccp2 como salida y y rd7 salida y siempre uno
   set_tris_D(0b01111011);
   //PIN D7
   output_high(PIN_D7);
   
   setup_ccp2(CCP_PWM);

   // interrupcion cada 32,7 ms TIMER 0
   setup_timer_0(RTCC_INTERNAL|RTCC_DIV_256);
   set_timer0(0);
   //configuramos tmr1 para contar pulsaciones ecternos e inicializamos a 0
   setup_timer_1(T1_EXTERNAL);
   set_timer1(0);
   //activar interrupciones 
   enable_interrupts(INT_RTCC);
   enable_interrupts(GLOBAL);
   



  //*** Configuraci�n inicial del driver del LCD
   LCDCON = 0b10010011; // Habilitado m�dulo y pines VLCD, fosc/8192 y multiplexaci�n 1/4
   LCDPS =  0b00100000; // Ondas tipo A, polarizaci�n 1/3, driver activo, prescaler = 1
   //Habilitaci�n de los pines de los segmentos, con 1 se habilitan, con 0 desactivados
   LCDSE0 = 0b01001111;   //Si hay un "1" es que tiene que estar activo
   LCDSE1 = 0b00001000;
   LCDSE2 = 0b11100001;
   //**

   //Al principio hay que borrar toda la pantalla, para ello hay que poner a 0 todos los bits de los
   //registros LCDDATA0 (posici�n 0x110) hasta LCDDATA11 (pos. 0x11B), con esos bits se activan los
   //segmentos y los iconos
   for (puntero_borrado=0x110;puntero_borrado<0x11c;puntero_borrado++)
      *puntero_borrado=0;
    //*******


   //Ahora se muestran los d�gitos por primera vez
   Ver_LCD();   //Llamamos a la funci�n que muestre el valor inicial en el LCD



   while (true)
   {

   
   
      resultado_AD = read_adc(ADC_READ_ONLY);
   
      set_pwm2_duty(resultado_AD); // Leer conv. A/D y guardar en 10 bits -> duty
      
      read_adc(ADC_START_ONLY);
      
      
      
   }
}

