////////////////////////////////////////////////////////////////////////////////////////
// Programa que recibe y emite mediante una comunicaci�n Serie As�ncrona
// con los pines RC6 (transmisi�n) y RC7 (recepci�n) y se comunica con el
// puerto serie de un PC, trabajando �ste como emulador de terminal. Entre el
// puerto del microcontrolador y el PC se intercala un circuito de adaptaci�n
// a RS232 (MAX232)
//
// Tambi�n se emplea una entrada anal�gica en RA0. Se muestra un mensaje y se sit�a en
// estado de espera a que se reciba un car�cter. Se detecta la recepci�n por interrupci�n
// se recoge el car�cter recibido y si es 'T' se lanza una conversi�n sobre RA0
// enviando un mensaje con el resultado obtenido (en V) a trav�s del puerto serie
//
////////////////////////////////////////////////////////////////////////////////////////

#include "Serie_RS232.h"

///////////////////Programa de tratamiento de la interrupci�n por recepci�n de un byte
#int_RDA
RDA_isr()
{
   int   caracter;
   long  conversion,tension;
   float tension_en_V;

//   if (OERR==TRUE){
//       CREN
//       
//   }
   
   if(kbhit())  caracter=getch();   //Si hay un caracter disponible lo recojo
   else        return;              //y si no salgo
   
   
   
   if(caracter=='T')    //Si el car�cter recibido es 'T'
   {
      conversion=read_adc();  //Recojo el valor de la conversi�n
      tension_en_V=(float)5*conversion/1024;  //Lo convierto en voltios

       printf("Tension = %1.4f V\n\r",tension_en_V); //Lo env�o
   }
   else {
        if (caracter=='\n' || caracter=='\r')
            return;
        else  //En caso contrario, se env�a otro mensaje de advertencia
            printf("Que introduzca una T para ver tension!!\n\r");

   }
   }

void main() {
   setup_oscillator(OSC_8MHZ);
   setup_adc_ports(sAN0|VSS_VDD);  //Configuramos RA0 como entrada anal�gica
   setup_adc(ADC_CLOCK_INTERNAL); //Usamos el reloj RC interno del conversor
   setup_spi(FALSE);
   setup_counters(RTCC_INTERNAL,RTCC_DIV_2);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_ccp2(CCP_OFF);
   enable_interrupts(INT_RDA);   //Activamos interrupciones por recepci�n
   enable_interrupts(global);    //y la m�scara global

   set_adc_channel(0);  //Seleccionamos RA0 como entrada anal�gica para el conversor

   printf("Introduzca una T para ver tension:\n\r");

   while(1);   //Bucle continuo del que salimos solo por interrupci�n al llegar caracter

}
//El codigo seria mejorable si se configurase el bucle para que cuando se desborde el buffer, leer el estado del bit que lo indica (oerr)
//y si esta a 1 se resetearia el cren (ponerlo a 0 y luego a 1) pa arreglarlo