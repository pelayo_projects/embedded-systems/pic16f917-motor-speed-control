////////////////////////////////////////////////////////////////////////////////////////
// Programa que recibe y emite mediante una comunicaci�n Serie As�ncrona
// con los pines RC6 (transmisi�n) y RC7 (recepci�n) y se comunica con el
// puerto serie de un PC, trabajando �ste como emulador de terminal. Entre el
// puerto del microcontrolador y el PC se intercala un circuito de adaptaci�n
// a RS232 (MAX232)
//
// Tambi�n se emplea una entrada anal�gica en RA0. Se muestra un mensaje y se sit�a en
// estado de espera a que se reciba un car�cter. Se detecta la recepci�n se recoge el
// car�cter recibido y si es 'T' se lanza una conversi�n sobre RA0
// enviando un mensaje con el resultado obtenido (en V) a trav�s del puerto serie
//
////////////////////////////////////////////////////////////////////////////////////////

#include "serie_rs232.h"

void main() {
   byte   caracter;
   long   conversion;
   float   tension_en_V;
   
   setup_oscillator(OSC_8MHZ);
   setup_adc_ports(sAN0|VSS_VDD);  //Configuramos RA0 como entrada anal�gica
   setup_adc(ADC_CLOCK_INTERNAL); //Usamos el reloj RC interno del conversor
   
   setup_spi(FALSE);
   setup_counters(RTCC_INTERNAL,RTCC_DIV_2);
   setup_timer_1(T1_DISABLED);
   setup_timer_2(T2_DISABLED,0,1);
   setup_ccp1(CCP_OFF);
   setup_ccp2(CCP_OFF);
   
   set_adc_channel(0);           //Seleccionamos el canal RA0 para conversi�n A/D


   while(1)
   {
      printf("\n\rIntroduzca una T para ver tension:\n\r");
      
      while(!kbhit());   //esperamos mientras no haya recepci�n de un car�cter
      caracter=getch();   //Si hay un caracter disponible lo recojo

         if(caracter=='T')  //Si el car�cter era 'T'
         {
            conversion=read_adc();  //Recojo el valor de la conversi�n
            tension_en_V=48*(float)conversion/10000;  //Lo paso a voltios

            printf("\n\rTension = %1.4f V\n\r",tension_en_V); //Lo muestro en el terminal
         }
         else
            printf("\n\rQue introduzca una T para ver tension!!\n\r");
   }          //Otra vez

}
